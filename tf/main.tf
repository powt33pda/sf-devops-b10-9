terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.68.0"
    }
  }
}

provider "yandex" {
  token     = "AQAAAAAeIycyAATuwYcHREtXjUSziaP5q0IUb5U"
  cloud_id  = "cloud-33pda"
  folder_id = "b1gk2ai7nhqsar0e9k55"
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "docker01" {
  name        = "docker01"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  hostname    = "docker01"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd80viupr3qjr5g6g9du"
      size = 20
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.sf-b9.id}"
    ipv4 = "true"
    nat = "true"
  }
  metadata = {
    ssh-keys = "ubuntu:${file("../keys/docker.pub")}"
    serial-port-enable: "1"
  }
}

resource "yandex_vpc_network" "sf-b9" { }
  
resource "yandex_vpc_subnet" "sf-b9" {
    zone       = "ru-central1-a"
    network_id = "${yandex_vpc_network.sf-b9.id}"
    v4_cidr_blocks = ["172.16.0.0/16"]
}